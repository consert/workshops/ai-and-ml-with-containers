"""Call the model in parallel, to check dynamic batching."""

import argparse
import asyncio
import json
import logging
import sys
from pathlib import Path
from time import perf_counter_ns
from typing import Any, List

import aiohttp

logging.basicConfig(level=logging.INFO, stream=sys.stdout)
LOG = logging.getLogger(__name__)

try:
    # lazy, let's re-use the existing cli from client.py
    from client import cli as client_cli
except ImportError:
    sys.path.append(str(Path(__file__).parent.parent))
    from client import cli as client_cli


def cli() -> argparse.ArgumentParser:
    """Command line interface."""
    parser = client_cli()
    parser.add_argument(
        "--no-calls",
        "-n",
        type=int,
        required=False,
        default=10,
        help="Number of calls to make",
    )
    return parser  # type: ignore[no-any-return]


async def call_model(
    call_id: int,
    inputs: List[str],
    model_name: str,
    base_url: str,
    session: aiohttp.ClientSession,
) -> Any:
    """Call the model."""
    url = f"{base_url}/v2/models/{model_name}/infer"
    headers = {"Content-Type": "application/json"}
    shape = [len(inputs), 1]
    data = json.dumps(
        {
            "id": f"{call_id}",
            "inputs": [
                {
                    "name": "texts",
                    "datatype": "BYTES",
                    "shape": shape,
                    "data": inputs,
                }
            ],
        }
    )
    # pylint: disable=too-many-try-statements
    try:
        async with session.post(url=url, headers=headers, data=data) as response:
            return await response.json()
    except aiohttp.ClientError as exc:
        LOG.error(exc)
        raise RuntimeError from exc


async def main() -> None:
    """Run the client."""
    args = cli().parse_args()
    if args.base_url.endswith("/"):
        args.base_url = args.base_url[:-1]
    if args.verbose:
        LOG.setLevel(logging.DEBUG)
    LOG.debug(args)
    async with aiohttp.ClientSession() as session:
        results = await asyncio.gather(
            *[
                call_model(
                    call_id=index,
                    inputs=args.inputs,
                    model_name=args.model_name,
                    base_url=args.base_url,
                    session=session,
                )
                for index in range(args.no_calls)
            ]
        )
    LOG.info("Results:\n%s\n", json.dumps(results, indent=4))


if __name__ == "__main__":
    start = perf_counter_ns()
    asyncio.run(main())
    end = perf_counter_ns()
    LOG.info("\nTook %s ms", (end - start) / 1_000_000)
