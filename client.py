"""Inference client for the pytriton server."""
import argparse
import json
import logging
import sys
from typing import Any, Dict, List

import requests

logging.basicConfig(level=logging.INFO, stream=sys.stdout)
LOG = logging.getLogger(__name__)


def cli() -> argparse.ArgumentParser:
    """Command line interface."""
    parser = argparse.ArgumentParser(description="Inference client for the pytriton server.")
    parser.add_argument(
        "inputs",
        type=str,
        nargs="+",
        help="Text(s) to classify",
    )
    parser.add_argument(
        "--base-url",
        type=str,
        required=False,
        default="http://localhost:8000",
        help="URL of the server",
    )
    parser.add_argument(
        "--model-name",
        type=str,
        required=False,
        default="MyModel",
        help="Name of the model to use",
    )
    parser.add_argument(
        "--verbose",
        action="store_true",
        help="Enable verbose logging",
    )
    return parser


def call_model(inputs: List[str], model_name: str, base_url: str) -> Dict[str, Any]:
    """Call the model."""
    url = f"{base_url}/v2/models/{model_name}/infer"
    headers = {"Content-Type": "application/json"}
    shape = [len(inputs), 1]
    data = json.dumps(
        {
            "id": "1",
            "inputs": [
                {
                    "name": "texts",
                    "datatype": "BYTES",
                    "shape": shape,
                    "data": inputs,
                }
            ],
        }
    )
    LOG.debug(data)
    response = requests.post(url, headers=headers, data=data, timeout=30)
    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError as err:
        LOG.error(err)
        raise RuntimeError from err
    results: Dict[str, Any] = response.json()
    return results


def main() -> None:
    """Parse arguments and run client."""
    args = cli().parse_args()
    if args.base_url.endswith("/"):
        args.base_url = args.base_url[:-1]
    if args.verbose:
        LOG.setLevel(logging.DEBUG)
    inputs = args.inputs
    model_name = args.model_name
    results = call_model(inputs, model_name, args.base_url)
    LOG.info("Results:\n%s\n", json.dumps(results, indent=4))


if __name__ == "__main__":
    main()
