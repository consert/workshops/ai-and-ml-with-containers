# Windows setup

## Install WSL2

<https://learn.microsoft.com/en-us/windows/wsl/install>

## Install Ubuntu (or any other preferred distro)

<https://apps.microsoft.com/detail/9PN20MSR04DW>

![Ubuntu](./ubuntu.png)

## Install Docker

<https://docs.docker.com/desktop/wsl/>
![docker_desktop](./docker_desktop.png)

## Or/and podman

<https://github.com/containers/podman/blob/main/docs/tutorials/podman-for-windows.md>
![podman](./podman.png)

## For gpu with wsl: install nvidia-container-toolkit

<https://docs.nvidia.com/cuda/wsl-user-guide/index.html>
<https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker>

## Vscode

Install the remote development extension for WSL

<https://code.visualstudio.com/docs/remote/remote-overview>

<https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack>

![vscode](./vscode.png)

## Clone the repo in WSL

```bash
mkdir -p ~/Projects && cd ~/Projects
git clone https://gitlab.com/consert/workshops/ai-and-ml-with-containers.git
```

![clone](./clone.png)

## Open the project in vscode

![project](./project.png)

```bash
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
python -m app.main
# on a new terminal
python client.py "Text to classify"
```

![run](./run.png)
![client](./client.png)

## Build the container

This step can also be done outside of vscode.

```bash
docker build -f Containerfile.cpu -t mymodel:cpu .
```

![docker](./docker.png)
