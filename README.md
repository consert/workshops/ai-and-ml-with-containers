# AI and ML with containers

Containerized ML model deployment.

Model: [Twitter-roBERTa-base for Sentiment Analysis](https://huggingface.co/cardiffnlp/twitter-roberta-base-sentiment-latest). Colab [link](https://colab.research.google.com/drive/1Nw0VVJxVujZ1XV06AVFrUulE4KbtF1NX?usp=sharing).  

## Requirements

Podman or Docker  
Podman: <https://podman.io/>  
Docker: <https://docs.docker.com/desktop/>

Python <3.12, >= 3.8 <https://www.python.org/downloads/> 

For local development, we will need Python >=3.8,< 3.12 and the requirements in `requirements.txt` (optionally, the requirements in `requirements-dev.txt`).

```shell
# clone the repo
git clone https://gitlab.com/consert/workshops/ai-and-ml-with-containers.git workshop
# or using ssh:
# git clone git@gitlab.com:consert/workshops/ai-and-ml-with-containers.git workshop
cd workshop
# create a virtual environment
# use python <3.12, >= 3.8
python3 -m venv .venv
# python3.10 -m venv .venv
# activate the virtual environment
##########
# source .venv/bin/activate
# or
# .venv\Scripts\activate.ps1
###########
# make sure we have the latest pip, setuptools and wheel versions
python3 -m pip install --upgrade pip setuptools wheel
# install the requirements
python3 -m pip install -r requirements.txt
# optionally, include the requirements-dev.txt
# pip install -r requirements.txt -r requirements-dev.txt
```

## Windows Setup

Nvidia-pytriton is not available on Windows, so we can either use FastAPI as an alternative, or use WSL2: [Windows Setup](./docs/windows.md)

## Container (docker/podman) commands

Build the container image(s):

```shell
# on OSX/arm64, we might also need to specify the platform:
# "docker build --platform linux/amd64 -f ..."
# docker/podman build ...
# cpu, python3.11
docker build -f Containerfile.cpu -t mymodel:cpu .
# cuda, python3.10
docker build -f Containerfile.cuda -t mymodel:cuda .
# cuda, python3.11
docker build -f Containerfile.py.x.cuda --build-arg PY_VERSION=3.11 -t mymodel:cuda-py3.11 .
# https://catalog.ngc.nvidia.com/orgs/nvidia/containers/cuda/tags
# cuda 12.3
docker build -f Containerfile.py.x.cuda \
    --build-arg BASE_IMAGE=nvcr.io/nvidia/cuda:12.3.0-devel-ubuntu22.04 \
    -t mymodel:cuda12.3 .
# jetson nano, python3.8, cuda 10.2
docker build -f Containerfile.jetson -t mymodel:jetson .
```

## Start a container

```shell
# docker/podman run ...
docker run --rm --name mymodel mymodel:cpu
# ctrl-c to stop

# wih a volume
docker volume create huggingface 2>/dev/null || true
# docker: the volume is owned by root, let's `chown` it
docker run --rm \
  -v huggingface:/home/user/.cache/huggingface \
  --entrypoint /bin/sh \
  --user root \
  mymodel:cpu \
  -c 'touch /home/user/.cache/huggingface/.init && chown -R user:user /home/user/.cache/huggingface'

# detached mode with -d
# --init to handle signals correctly and avoid zombie processes
# https://docs.docker.com/engine/reference/run/#specify-an-init-process
# https://triton-inference-server.github.io/pytriton/latest/deploying_in_clusters/#specify-container-init-process
docker run --rm \
    -v huggingface:/home/user/.cache/huggingface \
    --name mymodel mymodel:cpu \
    -d --init

# stop with
docker stop mymodel

# forward the http port
docker run --rm \
    -v huggingface:/home/user/.cache/huggingface \
    -d --init \
    --name mymodel \
    -p 8000:8000 \
    mymodel:cpu

# with gpu
# sudo nvidia-ctk runtime configure --runtime=docker
# https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html
docker run --rm  --init --runtime nvidia -p 8000:8000 mymodel:cuda-12.3

# for podman, specify the device
# sudo nvidia-ctk cdi generate --output=/etc/cdi/nvidia.yaml
# https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/cdi-support.html
podman run --rm  --init --device nvidia.com/gpu=all -p 8000:8000 mymodel:cuda-12.3

```

## Test calling the model

Once the container is running, you can test it with the client:

```shell
$> python3 client.py "I am so happy" "I am so sad" "I don't care"
{
    "model_name": "MyModel",
    "model_version": "1",
    "outputs": [
        {
            "name": "label",
            "datatype": "BYTES",
            "shape": [
                3
            ],
            "data": [
                "positive",
                "negative",
                "neutral"
            ]
        },
        {
            "name": "confidence",
            "datatype": "FP32",
            "shape": [
                3
            ],
            "data": [
                0.9786681532859802,
                0.8334012031555176,
                0.5974375605583191
            ]
        }
    ]
}
```
