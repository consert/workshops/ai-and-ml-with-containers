"""Application entrypoint."""
import sys
from pathlib import Path

try:
    from app.config import HTTP_PORT, PREFER_FASTAPI
except ImportError:
    sys.path.insert(0, str(Path(__file__).parent.parent))
    from app.config import HTTP_PORT, PREFER_FASTAPI

if PREFER_FASTAPI:
    from app.fastapi_server import main as server
else:
    # prefer pytriton, fallback to fastapi
    try:
        from app.pytriton_server import main as server
    except ImportError as err:
        try:
            from app.fastapi_server import main as server
        except ImportError as exp:
            raise RuntimeError(
                f"Could not import either pytriton or fastapi: {err}, {exp}"
            ) from exp


if __name__ == "__main__":
    server(http_port=HTTP_PORT)
