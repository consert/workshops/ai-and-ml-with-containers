"""Model related functions and variables."""
import os
from typing import Any, Dict, List

import numpy as np
import torch
from scipy.special import softmax
from transformers import AutoConfig, AutoModelForSequenceClassification, AutoTokenizer

# allow overriding the model to use if we want to.
MODEL_REPO = os.environ.get("MODEL_REPO", "cardiffnlp/twitter-roberta-base-sentiment-latest")
DEVICE = "cuda" if torch.cuda.is_available() else "cpu"
MODEL_NAME = "MyModel"
MODEL_VERSION = 1

tokenizer = AutoTokenizer.from_pretrained(MODEL_REPO)
config = AutoConfig.from_pretrained(MODEL_REPO)
model = AutoModelForSequenceClassification.from_pretrained(MODEL_REPO)
model = model.to(DEVICE)


def preprocess(text: str) -> str:
    """Preprocess text.

    replace @anyUser with "@user"
    replace http(s)://.... with "http"

    """
    new_text = []
    for part in text.split(" "):
        part = "@user" if part.startswith("@") and len(part) > 1 else part
        part = "http" if part.startswith("http") else part
        new_text.append(part)
    return " ".join(new_text)


def run_inference(texts: List[str], batch_size: int) -> Dict[str, Any]:
    """Run the inference."""
    result_labels = []
    result_probabilities = []
    if not texts:
        return {}
    encoded_input = tokenizer(
        texts,
        padding=True,
        # truncation=True,
        return_tensors="pt",
    ).to(DEVICE)
    output = model(**encoded_input)
    batched_scores = output[0].cpu().detach().numpy()
    for index in range(batch_size):
        scores = softmax(batched_scores[index])
        ranking = np.argsort(scores)
        ranking = ranking[::-1]
        label = config.id2label[ranking[0]]
        result_labels.append(label)
        result_probabilities.append(float(scores[ranking[0]]))
    return {"label": result_labels, "confidence": result_probabilities}
