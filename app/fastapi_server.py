"""Inference server using fastapi."""

from typing import Any, List, Literal, Tuple

try:
    from typing import Annotated
except ImportError:
    # py <3.9 ? Annotated was added in 3.9:
    # https://docs.python.org/3/library/typing.html#typing.Annotated
    from typing_extensions import Annotated  # type: ignore

import uvicorn
from fastapi import FastAPI
from fastapi.responses import RedirectResponse
from pydantic import BaseModel, ConfigDict, Field, model_validator

from app.model import MODEL_NAME, MODEL_VERSION, preprocess, run_inference

app = FastAPI()


class ModelInput(BaseModel):
    """The (pytriton) model input."""

    name: str = "sequence"
    datatype: str = "BYTES"
    # (batch_size, input_size) # input_size=1, one string
    shape: Tuple[Annotated[int, Field(ge=1, examples=[1])], Literal[1]]
    # on pytriton:
    # inputs=[
    #     Tensor(name="sequence", dtype=bytes, shape=(1,)),
    # ]
    data: List[str]

    @model_validator(mode="after")
    def check_batch_size(self) -> "ModelInput":
        """Validate input data."""
        shape = self.shape
        data = self.data
        if not shape:
            raise ValueError("Invalid shape supplied")
        if not data:
            raise ValueError("No data supplied")
        if len(data) != shape[0]:
            raise ValueError("Invalid data: shape[0] should be the data length")
        return self


class InferInput(BaseModel):
    """The inference/call input."""

    id: str
    # one "inputs" key
    inputs: Annotated[
        List[ModelInput],
        Field(
            min_length=1,
            max_length=1,
            examples=[
                [{"name": "sequence", "datatype": "BYTES", "shape": [1, 1], "data": ["hello"]}]
            ],
        ),
    ]


class ModelOutput(BaseModel):
    """The model's output  in the format pytriton uses."""

    name: str
    datatype: str
    # list with one int: [batch_size]
    shape: Annotated[
        List[Annotated[int, Field(ge=1, examples=[1])]],
        Field(min_length=1, max_length=1, examples=[[1]]),
    ]
    data: List[Any]

    @model_validator(mode="after")
    def check_batch_size(self) -> "ModelOutput":
        """Validate output data."""
        shape = self.shape
        data = self.data
        if not shape:
            raise ValueError("Invalid shape supplied")
        if not data:
            raise ValueError("No data supplied")
        if len(data) != shape[0]:
            raise ValueError("Invalid data: shape[0] should be the same with the data length")
        return self


class InferOutput(BaseModel):
    """The inference/call output."""

    id: str
    model_name: str = f"{MODEL_NAME}"
    model_version: str = f"{MODEL_VERSION}"
    outputs: Annotated[
        List[ModelOutput],
        Field(
            min_length=2,
            max_length=2,
            examples=[
                {"name": "label", "datatype": "BYTES", "shape": [1], "data": ["positive"]},
                {"name": "confidence", "datatype": "FP32", "shape": [1], "data": [0.9]},
            ],
        ),
    ]
    # on pytriton:
    # outputs=[
    #     Tensor(name="label", dtype=bytes, shape=(1,)),
    #     Tensor(name="confidence", dtype=np.float32, shape=(1,)),
    # ]
    model_config = ConfigDict(protected_namespaces=())


@app.get("/", include_in_schema=False)
async def root() -> RedirectResponse:
    """Redirect to docs."""
    return RedirectResponse(url="/docs")


@app.post(f"/v2/models/{MODEL_NAME}/infer")
def infer(body: InferInput) -> InferOutput:
    """Run model inference on a list of texts."""
    texts = body.inputs[0].data
    batch_size = len(texts)
    preprocessed_texts = [preprocess(text) for text in texts]
    model_output = run_inference(texts=preprocessed_texts, batch_size=batch_size)
    labels = model_output["label"]
    confidences = model_output["confidence"]
    label_outputs = ModelOutput(
        name="label",
        datatype="BYTES",
        shape=[len(labels)],
        data=labels,
    )
    confidence_outputs = ModelOutput(
        name="confidence",
        datatype="FP32",
        shape=[len(confidences)],
        data=confidences,
    )
    return InferOutput(id=body.id, outputs=[label_outputs, confidence_outputs])


def main(http_port: int) -> None:
    """Start the server."""
    uvicorn.run(app, host="0.0.0.0", port=http_port)  # nosemgrep # nosec
