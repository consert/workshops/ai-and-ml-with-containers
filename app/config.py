"""Server configuration."""
import os

_HTTP_PORT = os.environ.get("HTTP_PORT", "8000")
HTTP_PORT = int(_HTTP_PORT)

_PREFER_FASTAPI = os.environ.get("PREFER_FASTAPI", "false")
if not _PREFER_FASTAPI:
    _PREFER_FASTAPI = "false"
PREFER_FASTAPI = _PREFER_FASTAPI.lower()[0] in ("t", "y", "1")
