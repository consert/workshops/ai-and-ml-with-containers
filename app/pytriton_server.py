"""Inference server using pytriton."""
from typing import Dict

import numpy as np
import numpy.typing as npt
from pytriton.decorators import batch
from pytriton.model_config import Tensor
from pytriton.triton import Triton, TritonConfig

from app.model import MODEL_NAME, MODEL_VERSION, preprocess, run_inference


@batch
def infer_fn(texts: npt.NDArray[np.bytes_]) -> Dict[str, npt.NDArray[np.bytes_]]:
    """Inference call."""
    batch_size = texts.shape[0]
    text_strings_array = np.char.decode(texts.astype("bytes"), "utf-8")
    text_strings_list = text_strings_array.tolist()
    infer_inputs = []
    for text in text_strings_list:
        if isinstance(text, str):
            infer_inputs.append(preprocess(text=text))
        elif isinstance(text, list):
            infer_inputs.extend([preprocess(item) for item in text])
    results = run_inference(texts=infer_inputs, batch_size=batch_size)
    return {
        "label": np.char.encode(results["label"], encoding="utf-8"),
        "confidence": np.array(results["confidence"]),
    }


def main(http_port: int) -> None:
    """Start the server."""
    config = TritonConfig(http_port=http_port)
    with Triton(config=config) as triton:
        triton.bind(
            model_name=MODEL_NAME,
            model_version=MODEL_VERSION,
            infer_func=infer_fn,
            inputs=[Tensor(shape=(1,), dtype=bytes, name="texts")],
            outputs=[
                Tensor(shape=(1,), name="label", dtype=bytes),
                Tensor(name="confidence", dtype=np.float32, shape=(1,)),
            ],
        )
        triton.serve()
