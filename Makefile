.DEFAULT_GOAL := help

.PHONY: help
help:
	@echo "Usage: make [target]"
	@echo ""
	@echo "Default target: help"
	@echo ""
	@echo "Targets:"
	@echo "  help        	Show this help message"
	@echo "  start       	Start the server"
	@echo "  format      	Format the code"
	@echo "  lint        	Lint the code"
	@echo "  forlint     	Shortcut for 'make format && make lint'"
	@echo "  clean       	Clean unneeded files (__pycache__, .mypy_cache, etc.)"


.PHONY: start
start:
	python -m app.main

.PHONY: format
format:
	isort .
	autoflake --remove-all-unused-imports --remove-unused-variables --in-place .
	black --config pyproject.toml .
	ruff format --config pyproject.toml .

.PHONY: lint
lint:
	isort --check-only .
	black --config pyproject.toml --check .
	mypy --config pyproject.toml .
	flake8 --config=.flake8
	pydocstyle --config pyproject.toml .
	pylint --rcfile=pyproject.toml --output-format=text app
	bandit -c pyproject.toml -r .
	yamllint -c .yamllint.yaml .
	ruff check --config pyproject.toml .

.PHONY: forlint
forlint: format lint

.PHONY: clean
clean:
	rm -rf `find . -name __pycache__`
	rm -f `find . -type f -name '*.py[co]' `
	rm -f `find . -type f -name '*~' `
	rm -f `find . -type f -name '.*~' `
	rm -f `find . -type f -name '.coverage.*' `
	rm -rf .cache
	rm -rf .mypy_cache
	rm -rf .pytest_cache
	rm -rf .coverage
	rm -rf build
	rm -rf dist
	rm -rf *.egg-info
